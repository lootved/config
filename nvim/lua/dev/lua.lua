require("plugins.lsp").setuplsp("lua_ls", {
  settings = {
    Lua = {
      runtime = {
        version = "LuaJIT",
      },
      diagnostics = {
        globals = { "vim" },
      },
      telemetry = {
        enable = false,
      },
      format = {
        enable = true,
      },
      --[[workspace = {
        library = vim.api.nvim_get_runtime_file("", true),
      },--]]
    },
  },
})
