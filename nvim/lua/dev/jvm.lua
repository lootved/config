require("plugins.lsp").setuplsp("jdtls")

local aug = vim.api.nvim_create_augroup("jenkins", {})
for _, event in ipairs({ "BufRead", "BufNewFile " }) do
	vim.api.nvim_create_autocmd(event, {
		group = aug,
		pattern = "Jenkinsfile",
		callback = function()
			vim.bo.filetype = "groovy"
		end,
	})
end
