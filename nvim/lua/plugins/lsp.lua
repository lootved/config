require("mason-lspconfig").setup({
	ensure_installed = { "lua_ls" },
	max_concurrent_installers = 4,
})

local lspconfig = require("lspconfig")
local cmp_capabilities = require("cmp_nvim_lsp").default_capabilities()

local lsp_defaults = lspconfig.util.default_config
lsp_defaults.capabilities = vim.tbl_deep_extend("force", lsp_defaults.capabilities, cmp_capabilities)

local function setuplsp(lsp, config)
	lspconfig[lsp].setup({})
end

local function map_lsp_keys()
	local opts = { noremap = true, silent = true } --, buffer = bufnr }
	nmap("<space>e", vim.diagnostic.open_float, opts)
	nmap("[d", vim.diagnostic.goto_prev, opts)
	nmap("]d", vim.diagnostic.goto_next, opts)
	nmap("<space>q", vim.diagnostic.setloclist, opts)
	nmap("gd", ":lua vim.lsp.buf.definition()<cr>")
	nmap("gD", ":lua vim.lsp.buf.declaration()<cr>")
	nmap("gi", ":lua vim.lsp.buf.implementation()<cr>")
	nmap("gw", ":lua vim.lsp.buf.document_symbol()<cr>")
	nmap("gw", ":lua vim.lsp.buf.workspace_symbol()<cr>")
	nmap("gr", ":lua vim.lsp.buf.references()<cr>")
	nmap("gt", ":lua vim.lsp.buf.type_definition()<cr>")
	nmap("K", ":lua vim.lsp.buf.hover()<cr>")
	nmap("<c-k>", ":lua vim.lsp.buf.signature_help()<cr>")
	nmap("<leader>af", ":lua vim.lsp.buf.code_action()<cr>")
	nmap("<leader>rn", ":lua vim.lsp.buf.rename()<cr>")
	-- See `:help vim.lsp.*` for documentation on any of the below functions
	nmap("<C-k>", vim.lsp.buf.signature_help, opts)
	nmap("<space>ca", vim.lsp.buf.code_action, opts)
	nmap("<space>f", vim.lsp.buf.format, opts)
end

map_lsp_keys()

return {
	setuplsp = setuplsp,
}
