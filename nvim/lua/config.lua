require("mappings")
require("utils")

local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local g = vim.g -- a table to access global variables
local opt = vim.opt -- to set options

--copy to clipboard
vim.api.nvim_set_option("clipboard", "unnamedplus")
cmd("set colorcolumn=80")
cmd("set wrap")
cmd("set linebreak")
cmd("set spelllang=en")
cmd("set spell")
cmd("set spellsuggest=best,9")

opt.completeopt = { "menuone", "noinsert", "noselect" } -- Completion options (for deoplete)
opt.expandtab = true -- Use spaces instead of tabs
opt.hidden = true -- Enable background buffers
opt.ignorecase = true -- Ignore case
opt.joinspaces = false -- No double spaces with join
opt.list = true -- Show some invisible characters
opt.number = true -- Show line number
opt.relativenumber = true -- Show relative line numbers
opt.scrolloff = 4 -- Lines of context
opt.shiftround = true -- Round indent
opt.shiftwidth = 2 -- Size of an indent
opt.sidescrolloff = 8 -- Columns of context
opt.smartcase = true -- Do not ignore case with capitals
opt.smartindent = true -- Insert indents automatically
opt.splitbelow = true -- Put new windows below current
opt.splitright = true -- Put new windows right of current
opt.tabstop = 2 -- Number of spaces tabs count for
opt.termguicolors = true -- True color support
opt.wildmode = { "list", "longest" } -- Command-line completion mode
opt.wrap = true
opt.cursorline = true
opt.cursorcolumn = true
opt.title = true
opt.titlestring = "%t"
opt.list = true
opt.termguicolors = true
opt.scrolloff = 10 --
opt.incsearch = true
opt.hlsearch = false --remove highligh after search

--if is_dev_env() then
local aug = vim.api.nvim_create_augroup("autoformat", {})
vim.api.nvim_create_autocmd("BufWritePre", {
	group = aug,
	pattern = "*",
	callback = function()
		vim.lsp.buf.format()
	end,
})
--end
--opt.listchars:append("space:⋅") --show spaces as dot
--opt.listchars:append("eol:↴") -- show \n as custom char
