function map(mode, lhs, rhs, opts)
	local options = { noremap = true }
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	if "string" == type(rhs) then
		vim.api.nvim_set_keymap(mode, lhs, rhs, options)
	else
		vim.keymap.set(mode, lhs, rhs, options)
	end
end

function nmap(k, v, opts)
	map("n", k, v, opts)
end

function imap(k, v, opts)
	map("i", k, v, opts)
end

function nmap_cmd(k, v, opts)
	nmap(k, ":" .. v .. "<CR>", opts)
end

-- Map a key to both normal and interactive mode
function map_cmd(k, v, opts)
	if type(v) == "string" then
		imap(k, "<ESC>:" .. v .. "<CR>", opts)
		nmap_cmd(k, v, opts)
	elseif type(v) == "function" then
		imap(k, v, opts)
		nmap(k, v, opts)
	end
end

map("", "<leader>c", '"+y') -- Copy to clipboard in normal, visual, select and operator modes
imap("<C-u>", "<ESC>ui<CR>")
nmap("<C-u>", "u")
-- <Tab> to navigate the completion menu
imap("<S-Tab>", 'pumvisible() ? "\\<C-p>" : "\\<Tab>"', { expr = true })
imap("<Tab>", 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', { expr = true })

imap("<C-z>", "<C-o>:u<CR>")
imap("<C-v>", "<C-r>+")

-- CTRL Q quit
map_cmd("<C-q>", "q")

-- Save with CTRL-S
map_cmd("<C-s>", "update")

map_cmd("<C-l>", "noh") -- Clear highlights
-- disable all auto commands
map_cmd("<C-y>", "set ei=All")

-- navigate open tabs
nmap("<C-j>", "<C-w>j")
nmap("<C-h>", "<C-w>h")
nmap("<C-k>", "<C-w>k")
nmap("<C-l>", "<C-w>l")
-- Fuzzy matching
nmap("<leader>ff", "<cmd>Telescope find_files<cr>")
nmap("<leader>fg", "<cmd>Telescope live_grep<cr>")
nmap("<leader>fb", "<cmd>Telescope buffers<cr>")
nmap("<leader>fh", "<cmd>Telescope help_tags<cr>")

-- spell fix
-- <C-x> then s

function RunCurrentFile()
	if "lua" == vim.bo.filetype then
		vim.cmd("source %")
	end
end

map_cmd("<F9>", RunCurrentFile)

nmap_cmd("<leader><space>", "Ex")
nmap_cmd("<leader>v", "vsplit")
nmap_cmd("<leader>h", "split")

nmap_cmd("<leader>l", "!cargo clippy")
