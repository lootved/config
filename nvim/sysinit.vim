" sourced from XDG_CONFIG_DIRS
let $ppath = "$SOFTWARE_CACHE/nvim/plugins/*"
set rtp+=$ppath
execute "luafile " "${NVIM_DIR}/init.lua"
