pcall(require, 'config')

-- Uncomment to automatically download any new plugin
--vim.env.NVIM_FORCE_INIT = true

require('bootstrap').run()
require('plugins').load()
prequire('dev')
